#!/usr/bin/env raku

sub cv-suffix(Str $root, Str $ac, Str $av) {
  $root ~ ($root ~~ / <[aiueo]>$ / ?? $av !! $ac);
}

sub vx-suffix(Str $root, Str $v, Str $ac, Str $av) {
  $root ~ ($root ~~ / $v$ / ?? $av !! $ac);
}

constant $IGNORE_TAGS = set (
  # Blacklisted parts of speech:
  "ユマナ", # yumana terms
  "地球の国名",
  "接頭辞", # affixes
  "接尾辞",
  "接辞",
  # Onomatopoeias and mimetic words (exclude?)
  # "擬音",
  # "擬声",
  # "演繹音",
  # "擬態語",
  # "擬音語",
  # Do not contain definitions
  "レベル", # word level
  "アクセント", # stress
  "語法", # usage
  "文化", # culture
  "類義語", # synonyms
  "反意語", # antonyms
  "類音", # similar-sounding words
  "注釈", # remarks
);

constant $ABBRS = set (
  # Entries excluded as abbreviations
  # Era abbreviations:
  "fl", # fial
  "tm", # artem
  "vs", # vaste
  "sr", # saria
  "cv", # cavas
  "zg", # azger
  "mt", # mertena
  "kk", # kakko
  "sm", # selmel
  "rd", # ordin
  "nd", # nadia
  "rt", # artil
  "vl", # velei
  # aleiyu → al, included meaning ‘to’
  "lj", # lanj
  # Language abbreviations:
  "n", # altiaren; ‘alt’ included meaning ‘other’
  "ard", "arf", "arv", # arbaren → ar, included meaning ‘turn on’
  "ad", "af", "av", # arka → a, included meaning ‘to’
  "frf", "frff", "frfs", # farfaniaren
  "f", # fiiliaren
  "fv", # filveeyu
  "ls", "lsd", "lsk", "lsl", "lss", "lsv", # lestilren
  "ly", # lyudiaren
  "sz", "szd", "szk", "szl", "szs", "szv", # siiziaren
  "lt", "ltd", "ltf", "ltk", "lts", "ltv", # tiaren
  # (modifiers)
  "d", # dist (‘old’)
  "f", # felm (‘middle’)
  "s", # sam (‘new’)
  "v", # vland (‘near’)
  "k", # klenz (‘far’)
  # (found in usage notes. Not all of these actually have an entry.)
  "rk", # ルカリア語 [rukaliaren]
  # "ard", # アルディアル語 [ardialren] / 古アルバザード語 [sidarbaren]
  "rf", # アルフィ語 [arfiren]
  "lz", # リーゼル語 [liizelren]
  "hy", # ヒュート語 [hyuutoren]
  "kt", # ケートイア語 [keetoiaren]
  # イネアート語 [ineaatoren] → in, included meaning ‘look at’
  "sk", # スカルディア語 [skaldiaren]
  "md", # メディアン語 [medianren]
  # メティオ語 [metioren] → me, included meaning ‘again’
  # アルカ・エ・ソーン [arka e sorn] → sorn, included because ‘sorn’ is used as a full word
  "pr", # プレディス語 [predisren]
  "fg", # フィガン語 [figanren]
  "vx", # ヴェルシオン語 [velxionren]
  "bg", # ベルガンド語 [belgandren]
  "dm", # ディミニオン語 [diminionren]
  "vm", # ヴェマ語 [vemaren]
  "lls", # ロロス語 [lolosren]
  # Units of measure
  "b", # melba
  "fm", # fainmelfi
  # mela → l, included as alternate form of le
  "m", # melfi
  "mm", # meinmelfi
  "nb", # noismelba
  "nl", # noismela
  "nm", # noismelfi
  # Other
  "dk", # diaklel
);

sub MAIN(Str $input, Str $output) {
  my $valid-words = SetHash.new;
  for $input.IO.lines -> $line {
    if $line ~~ / ^ (.+?)('#'?) \s+ '///' \s+ '/' (.*) $ / {
      my $word = ~$0;
      my $vulgar = $1 ne "";
      my @def = $2.split(/ \s* '\\' \s* /);
      while @def[* - 1] eq "" {
        @def.pop;
      }
  
      next if $word !~~ / ^ (<[a..z]>+) ['(' \d+ ')']? $ /;
      my $base-word = ~$0;
      next if $ABBRS{$base-word}; # Skip abbreviations
  
      my ($is-valid, $is-verb, $is-adj, $is-adv, $is-modal) = False xx *;
      for @def -> $def-line {
        my @pos-tags = ($def-line ~~ / ^ \s* ('［' (.+?) '］')* /)[0].map(~*[0]);
        next if !@pos-tags || @pos-tags (&) $IGNORE_TAGS;
        $is-valid = True;
        if "動詞" (elem) @pos-tags {
          $is-verb = True;
        } elsif "形容詞" (elem) @pos-tags {
          $is-adj = True;
        } elsif ("副詞", "遊離副詞") (&) @pos-tags {
          $is-adv = True;
        } elsif "法副詞" (elem) @pos-tags {
          $is-modal = True;
        }
      }
      if $is-valid {
        $valid-words.set($base-word);
        # The following derivations are based on the stemming algorithm used
        # by the Vulgar Arka Dictionary. Of course, we’re building the derived
        # forms from the stem instead of the other way around.
        # ismklel source: http://mindsc.ape.jp/klel/vanz_cgi.cgi
        if $is-verb {
          $valid-words.set((
            cv-suffix($base-word, "at", "t"), # Past tense
            cv-suffix($base-word, "or", "r"), # Progressive aspect
            cv-suffix($base-word, "ik", "k"), # Perfective aspect
            cv-suffix($base-word, "es", "s"), # Continuous aspect
            cv-suffix($base-word, "and", "nd"), # Habitual aspect
            cv-suffix($base-word, "ast", "st"), # Reflexive participle
            cv-suffix($base-word, "ont", "nt"), # Natural participle
            vx-suffix($base-word, "a", "an", "n"), # Active participle
            vx-suffix($base-word, "o", "ol", "l"), # Passive participle
            cv-suffix($base-word, "el", "l"), # ACC converb
            vx-suffix($base-word, "a", "anel", "nel"), # NOM converb
            cv-suffix($base-word, "ator", "tor"), # Past progressive
            cv-suffix($base-word, "atik", "tik"), # Past perfective
            cv-suffix($base-word, "ates", "tes"), # Past continuous
            cv-suffix($base-word, "atand", "tand"), # Past habitual
          ));
        }
        if $is-adj && !$is-modal {
          $valid-words.set((
            vx-suffix($base-word, "a", "an", "n"), # Active participle
            vx-suffix($base-word, "o", "ol", "l"), # Passive participle
            cv-suffix($base-word, "el", "l"), # Adverb
            cv-suffix($base-word, "ati", "ti"), # Quality noun
            cv-suffix($base-word, "en", "n"), # Genitive
          ));
        }
      }
    } else {
      die "Could not parse line: $line"
    }
  }
  my @sorted-words = $valid-words.keys.sort;
  my $out = open :w, $output;
  for @sorted-words -> $word {
    $out.say($word);
  }
}

#!/usr/bin/env raku

sub filt-wordle(Str $s) {
  return $s.chars == 5 && !$s.contains("q");
}

constant %MODES = (
  :wordle((&filt-wordle, "shuffle")),
);

sub MAIN(Str $input, Str $output, Str $mode, Bool :$use-json) {
  my ($pred, $op) = %MODES{$mode};
  my @words = $input.IO.lines.grep($pred);
  given $op {
    when "none" {}
    when "shuffle" {
      @words = @words.pick(*);
    }
    default {
      die "Unknown operation $op";
    }
  }
  my $out = open :w, $output;
  if $use-json {
    $out.say("[{@words.map({"\"$_\""}).join(",")}]");
  } else {
    for @words -> $word {
      $out.say($word);
    }
  }
}

#set text(lang: "art")
#set smartquote(enabled: false)

#let hacm = true
#let heading-font = if hacm { "fialis" } else { "linux libertine" }
#let body-font = if hacm { "kardinal" } else { "linux libertine" }

#show heading: set text(font: heading-font, size: 24pt)
#set text(font: body-font)

#let tesket = "tkxsnvfmdgpbhycrzjwlaioeu".codepoints()
#let tesket-rev = {
  let res = (:)
  for (i, c) in tesket.enumerate() {
    res.insert(c, str.from-unicode(97 + i))
  }
  res
}

#let veteda = {
  read("../veteda.txt")
    .split("\n")
    .filter(vet => vet.len() >= 2 and not vet.contains("q"))
    .sorted(key: vet => vet.codepoints().map(c => tesket-rev.at(c)).join())
}
#let veteda-dict = {
  let res = (:)
  for (i, c) in veteda.enumerate() {
    res.insert(c, i)
  }
  res
}

= vetliva t'arka

== 2 hacm

#let hacm2 = veteda.filter(vet => vet.len() == 2)

#grid(columns: 24, gutter: 0.5em, ..hacm2)

== 3 hacm

#{
  set text(size: 0.65em)
  grid(columns: 28, gutter: 0.5em, ..veteda.filter(vet => vet.len() == 3))
}

== cuk (alkalten 5 hacm)

#grid(columns: 14, gutter: 0.5em, ..veteda.filter(vet => vet.contains("c") and vet.len() <= 5).sorted(key: vet => vet.len()))

== jok (alkalten 4 hacm)

#grid(columns: 16, gutter: 0.5em, ..veteda.filter(vet => vet.contains("j") and vet.len() <= 4).sorted(key: vet => vet.len()))

== wit (alkalten 4 hacm)

#grid(columns: 16, gutter: 0.5em, ..veteda.filter(vet => vet.contains("w") and vet.len() <= 4).sorted(key: vet => vet.len()))

== vesto (alkalten 5 hacm)

#let required-vowels = (0, 1, 2, 3, 3, 4)
#let vesto = regex("[aioeu]")

#grid(columns: 15, gutter: 0.5em, ..veteda.filter(vet => vet.len() < required-vowels.len() and vet.matches(vesto).len() >= required-vowels.at(vet.len())).sorted(key: vet => vet.len()))

== halp e vet oken 2 hacm

#let halp = hacm2.map(vet => {
  let lank = ""
  let mik = ""
  for c in tesket {
    if veteda-dict.at(c + vet, default: none) != none {
      lank += c
    }
    if veteda-dict.at(vet + c, default: none) != none {
      mik += c
    }
  }
  (text(lank, size: 0.8em, tracking: 0.4pt), vet, text(mik, size: 0.8em, tracking: 0.4pt))
})

#align(center)[
  #table(
    columns: (auto, auto, auto, auto, auto, auto),
    inset: 6pt,
    stroke: none,
    align: (right + bottom, center + bottom, left + bottom, right + bottom, center + bottom, left + bottom),
    ..{
      let rows = calc.ceil(halp.len() / 2);
      range(halp.len()).map(i => {
        halp.at(calc.div-euclid(i, 2) + if calc.even(i) { 0 } else { rows })
      }).flatten()
    }
  )
]

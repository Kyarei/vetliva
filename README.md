# veteda

## How to build the wordlist

```bash
wget https://mindsc.ape.jp/klel/arka.dat
raku extract.raku arka.dat veteda.txt
```

## How to build the cheatsheet

```bash
typst compile cheatsheet/cheatsheet.typ --font-path cheatsheet/ --root .
```
